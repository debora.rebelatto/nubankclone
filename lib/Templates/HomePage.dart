
import 'package:flutter/material.dart';
import 'package:nubank_clone/Templates/Overview.dart';
import 'package:nubank_clone/Templates/Settings.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _obscure = true;
  var isSettings = false;

  void _toggle() {
    setState(() {
      _obscure = !_obscure;
    });
  }

  void _changePage() {
    setState(() {
      isSettings = !isSettings;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Container(
          padding: EdgeInsets.only(left: 15),
          child: Text('Olá, Débora', style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold
          )),
        ),

        toolbarHeight: 80,
        actions: [
          !isSettings
            ? Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: RawMaterialButton(
                elevation: 0,
                fillColor: Color(0xff9824c7),
                child: Icon(_obscure ? Icons.visibility : Icons.visibility_off),
                shape: CircleBorder(),
                onPressed: _toggle,
              ),
            ) : Container(),

            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: RawMaterialButton(
                elevation: 0,
                fillColor: Color(0xff9824c7),
                child: Icon(!isSettings ? Icons.settings : Icons.close),
                shape: CircleBorder(),
                onPressed: _changePage,
              ),
            )
        ],
      ),

      body: isSettings ? Settings() : OverView(_obscure)

    );
  }
}
