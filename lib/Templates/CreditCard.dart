import 'package:flutter/material.dart';

class CreditCard extends StatefulWidget {
  @override
  _CreditCardState createState() => _CreditCardState();
}

class _CreditCardState extends State<CreditCard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: Material(
          color: Colors.white,
          child: Ink(
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                padding: EdgeInsets.only(left: 5),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios, color: Colors.black, size: 20),
                  Icon(Icons.credit_card, color: Colors.black, size: 20,)
                ],),
              ),
            ),
          ),
        ),

        actions: [
          IconButton(icon: Icon(Icons.search, color: Colors.black,), onPressed: () {}),
          IconButton(icon: Icon(Icons.help, color: Colors.black,), onPressed: () {})
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 20),
                    width: MediaQuery.of(context).size.width * 0.9,
                    // color: Colors.blue,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Fatura Atual', style: TextStyle(color: Colors.blue, fontSize: 15)),
                        SizedBox(height: 10),
                        Text('R\$ 123,00', style: TextStyle(color: Colors.blue, fontSize: 20)),
                        SizedBox(height: 10),

                        Row(children: [
                          Text('Limite disponível ', style: TextStyle(color: Colors.grey[700], fontSize: 15)),
                          Text('R\$ 123,00', style: TextStyle(color: Colors.green[900], fontSize: 15)),
                        ],)
                      ],
                    ),
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width * 0.1,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("here's that bar")

                        ],
                      ),
                    )
                  ),
                ],
              ),
            ),

            // Container(
            //   height: 130,
            //   color: Colors.blue,
            //   child: ListView(
            //     shrinkWrap: true,
            //     scrollDirection: Axis.horizontal,
            //     children: [
            //       ListTile(
            //         leading: Icon(Icons.ac_unit),
            //         title: Text('kljsad'),
            //       ),
            //       ListTile(
            //         leading: Icon(Icons.ac_unit),
            //         title: Text('kljsad'),
            //       ),
            //       ListTile(
            //         leading: Icon(Icons.ac_unit),
            //         title: Text('kljsad'),
            //       ),
            //       ListTile(
            //         leading: Icon(Icons.ac_unit),
            //         title: Text('kljsad'),
            //       )
            //     ],
            //   )
            // ),

            infoTile(),
            infoTile(),
            infoTile(),
            infoTile(),
            infoTile(),
            infoTile(),
            infoTile(),
            infoTile(),

          ]
        ),
      )
    );
  }

  Widget infoTile() {
    return ListTile(
      leading: Icon(Icons.ac_unit),
      title: Text('what'),
      subtitle: Text('where'),
      trailing: Text('Domingo'),
    );
  }
}