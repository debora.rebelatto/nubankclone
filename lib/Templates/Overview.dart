import 'package:flutter/material.dart';
import 'package:nubank_clone/Templates/CreditCard.dart';

// ignore: must_be_immutable
class OverView extends StatefulWidget {
  bool obscure;
  OverView(this.obscure);
  @override
  _OverViewState createState() => _OverViewState();
}

class _OverViewState extends State<OverView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            cardCreditCard(),
            cardAccount(),
            cardEmprestimo()

          ]
        )
      ),
      bottomNavigationBar: Container(
        height: 130,
        child: ListView(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          children: [
            item('Pix', Icons.group_add),
            item('Pagar', Icons.help),
            item('Indicar Amigos', Icons.phone),
            item('Transferir', Icons.search),
            item('Depositar', Icons.search),
            item('Empréstimos', Icons.search),
            item('Cartão virtual', Icons.search),
            item('Recarga de celular', Icons.search),
            item('Ajustar limite', Icons.search),
            item('Bloquear cartão', Icons.search),
            item('Cobrar', Icons.search),
            item('Doação', Icons.search),
            item('Me ajuda', Icons.search),
          ],
        )
      ),
    );
  }


  Widget item(label, icon) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        height: 100,
        width: 100,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Color(0xff9824c7),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(2, 0, 0, 10),
              child: Icon(icon, color: Colors.white,),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(2, 0, 0, 10),
              child: Text(label, style: TextStyle(color: Colors.white),),
            )
          ],
        ),
      )
    );
  }


  Widget cardCreditCard() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Card(
        child: Material(
          child: Ink(
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => CreditCard())
                );
              },
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    header(Icons.credit_card, 'Cartão de Crédito'),

                    SizedBox(height: 10),
                    Text('Fatura atual', style: TextStyle(color: Colors.grey[600])),
                    SizedBox(height: 10),

                    widget.obscure
                    ? Container(
                      height: 60,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('R\$ 123,00',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.blue
                            ),
                          ),
                          SizedBox(height: 10),

                          Row(children: [
                            Text(
                              'Limite disponível ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.grey[700],
                                fontSize: 15
                              ),
                            ),
                            Text('R\$ 123,00',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: Colors.green[900]
                              )
                            )
                          ],)
                        ],
                      )
                    )
                    : Container(
                      height: 60,
                      color: Colors.grey[200]
                    )
                  ],
                ),
              )
            ),
          ),
        )
      )
    );
  }

  cardAccount() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Card(
        child: Material(
          child: Ink(
            child: InkWell(
              onTap: () {

              },
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    header(Icons.money, 'Conta'),

                    SizedBox(height: 10),

                    Text('Saldo disponível', style: TextStyle(color: Colors.grey[700]),),

                    SizedBox(height: 10),

                    widget.obscure
                      ? Container(
                        height: 40,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('R\$ 123,00',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        )
                      )
                      : Container(
                        height: 40,
                        color: Colors.grey[200]
                      )
                  ],
                ),
              )
            )
          )
        )
      )
    );
  }

  Widget cardEmprestimo() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Card(
        child: Material(child: Ink(child: InkWell(
          onTap: () {print('kajsd.a,jd');},
          child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: [
                Icon(Icons.monetization_on, color: Colors.grey[600],),
                SizedBox(width: 10),
                Text('Empréstimo', style: TextStyle(color: Colors.grey[600])),
              ],),

              SizedBox(height: 15),

              widget.obscure
                ? Container(
                  height: 40,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('R\$ 123,00',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  )
                )
                : Container(
                  height: 40,
                  color: Colors.grey[200]
                ),

                SizedBox(height: 10),

                OutlineButton(
                  child: Text('Simular empréstimo', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold),),
                  borderSide: BorderSide(color: Colors.purple),
                  onPressed: () {},
                )
            ],
          ),
        )
      ))))
    );
  }

  header(icon, label) {
    return Row(children: [
      Icon(icon, color: Colors.grey[600],),
      SizedBox(width: 10),
      Text(label, style: TextStyle(color: Colors.grey[600])),
    ],);
  }
}