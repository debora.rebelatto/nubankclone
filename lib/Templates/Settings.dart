import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: [
            Center(
              child: Column(
                children: [
                  Text('Agência 0001 Conta xxxxxx-x', style: TextStyle(color: Colors.white),),
                  Text('Banco xxx - Nu Pagamentos S.A.', style: TextStyle(color: Colors.white),)
                ],
              )
            ),

            SizedBox(height: 20),

            Divider(color: Colors.white,),

            tiles(Icons.help, 'Me ajuda'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Perfil'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Configurar conta'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Minhas chaves Pix'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Configurar Cartão'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Pedir conta PJ'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Configurar Notificações'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Configurações do app'),
            Divider(color: Colors.white,),
            tiles(Icons.help, 'Sobre'),
            Divider(color: Colors.white,),

            SizedBox(height: 20),

            OutlineButton(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text('SAIR DO APP',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              borderSide: BorderSide(color: Colors.white),
              onPressed: () {},
            )

          ],
        )
      )
    );
  }


  tiles(icon, title) {
    return ListTile(
      leading: Icon(icon, color: Colors.white,),
      title: Text(title, style: TextStyle(color: Colors.white)),
      trailing: Icon(Icons.arrow_forward_ios, color: Colors.white),
    );
  }
}