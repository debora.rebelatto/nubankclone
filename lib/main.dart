import 'package:flutter/material.dart';
import 'package:nubank_clone/Templates/HomePage.dart';

void main() {
  runApp(NubankClone());
}

class NubankClone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        scaffoldBackgroundColor: Color(0xff8605b8),
        backgroundColor: Color(0xff8605b8)
      ),
      home: MyHomePage(),
    );
  }
}
